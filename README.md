# Minimal Godot Engine Docker image

This image is based on the slim version of the Debian jessie image. It provides the official Linux server builds of Godot Engine along with the official templates of version 3.0.6

The primary application is for automating Godot builds.

Godot Engine is located at ```/bin/godot``` and the templates are installed in ```/root/.local/share/godot/templates/3.0.6.stable/```.

### Usage example (Gitlab CI)

The source repository contains a ```Vagrantfile``` who provides Docker and Gitlab Runner. It can be used to test your Gitlab CI configuration (see ```.gitlab-ci.yml``` in the source repository):

    $ gitlab-runner exec docker <job name>
   
There is no need to register the runner first.

### Credits

Rafał Zawadzki: https://hub.docker.com/r/bluszcz/godot/
Mattias Cibien: https://github.com/mattiascibien/godot-docker
Quin Snider: https://github.com/StaticDynamics/godot-docker
